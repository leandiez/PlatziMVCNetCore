namespace platzi_mvc_netcore.Models
{
    public class Escuela{
        public string EscuelaId { get; set;}
        public string Nombre  { get; set;}
        public int AnioFundacion { get; set;}
    }
}