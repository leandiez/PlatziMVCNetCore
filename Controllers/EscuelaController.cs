using Microsoft.AspNetCore.Mvc;
using platzi_mvc_netcore.Models;

namespace platzi_mvc_netcore.Controllers;

public class EscuelaController : Controller{

    public IActionResult Index()
    {
        var escuela = new Escuela();
        escuela.Nombre = "Academia Platzi";
        escuela.AnioFundacion = 2022;
        escuela.EscuelaId = Guid.NewGuid().ToString();
        return View(escuela);
    }


}